<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="" class="logo">
 
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Home</b></span>
        </a>
     
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">

          <!-- Sidebar toggle button-->
       
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
            
          
         
                
              <a href="http://heartdiseasepredictionsystem.info"  class="logo">
 
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg" ><b>Logout</b></span>
        </a>
            
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            
          
            <li class="treeview">
              <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Take the Test Now</span>
               
              </a>
             
            </li>

       
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
   
         
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-3">
            </div>
            <div class="col-md-6">
              <!-- general form elements -->

             <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Take the Test Now </h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action = "../../../resources/views/templates/system/result.blade.php">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you have high blood pressure or are you taking a blood pressure medication?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a1" value="1"  >Yes&emsp;
                        <input type="radio"   name="a1"  value="0">No

                      </div>

                    </div>

                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">What are your smoking habits?</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a2" value="1"  >I currently smoke
&emsp;
                        <input type="radio"   name="a2"  value="0">I have never smoked


                      </div>

                    </div>

                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">What are your smoking habits?</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a3" value="1"  >I currently smoke
&emsp;
                        <input type="radio"   name="a3"  value="0">I have never smoked


                      </div>

                    </div>


                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you have high cholesterol or are taking a cholesterol lowering medication?
?</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a4" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a4"  value="0">No


                      </div>

                    </div>


                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you have diabetes?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a5" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a5"  value="0">No


                      </div>

                    </div>


                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Have you ever had a heart attack or stroke?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a6" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a6"  value="0">No


                      </div>

                    </div>


                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Have you ever had any heart procedures, such as stents, balloon angioplasty or bypass surgery?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a7" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a7"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Has anyone in your immediate family (father, mother, sibling) had a heart attack?</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a8" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a8"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">How would you describe your level of physical activity?</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a9" value="1"  >Not Active
&emsp;
                        <input type="radio"   name="a9"  value="0">Very Active


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you follow a low fat, low carb or vegetarian diet?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a10" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a10"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you eat red meat, packaged foods, fast food, or fried food 3 or more times per week?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a11" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a11"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you eat fish 3 or more times per week or take a daily fish oil supplement?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a12" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a12"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you currently have any periodontal diseases such as gingivitis?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a13" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a13"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you take aspirin daily?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a14" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a14"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you suffer from joint pain or arthritis?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a15" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a15"  value="0">No


                      </div>

                    </div>



                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you have asthma?
</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a16" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a16"  value="0">No


                      </div>

                    </div>

                 <div class="form-group">
                      <label for="inputEmail3" class="col-sm-12" style ="text-align:center">Do you suffer regular digestion problems including acid reflux?

</label>
                      <div class="col-sm-12" style ="text-align:center">
                        <input type="radio"   name="a17" value="1"  >Yes
&emsp;
                        <input type="radio"   name="a17"  value="0">No


                      </div>

                    </div>




                                  
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info pull-right">Submit</button>
                  </div><!-- /.box-footer -->
                </form>
              </div>

              <div class="col-md-3">
              </div>
       
        


     

            </div><!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-6">
   
    
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.1.0
        </div>
        <strong>Copyright &copy; 2016-2017 <a href=""> SUMAIYA BINTE HASAN & REHENUMATABASSUM MIFRA</a>.</strong> All rights reserved.
      </footer>

      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
