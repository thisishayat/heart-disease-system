<header class="main-header">
    <!-- Logo -->
    <a href="" class="logo">

        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Home</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">




                <a href="http://128.199.140.188/intern-project-html"  class="logo">

                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg" ><b>Logout</b></span>
                </a>

            </ul>
        </div>
    </nav>
</header>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">


            <li class="treeview">
                <a href="/app/sigin">
                    <i class="fa fa-files-o"></i>
                    <span>Take the Test Now</span>

                </a>

            </li>
            <li class="treeview">
                <a href="/app/doctor-help">
                    <i class="fa fa-files-o"></i>
                    <span>Doctor's Help</span>

                </a>

            </li>

            <li class="treeview">
                <a href="/app/chat-with-doctor">
                    <i class="fa fa-files-o"></i>
                    <span>Chat with Doctor</span>

                </a>

            </li>

            <li class="treeview">
                <a href="/chat-with-patient">
                    <i class="fa fa-files-o"></i>
                    <span>Chat with Patient - Doctor area</span>

                </a>

            </li>


        </ul>
    </section>
    <!-- /.sidebar -->
</aside>