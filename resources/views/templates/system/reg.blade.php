<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Login to Panel</title>

    <link rel="stylesheet" href="{{ asset('assets-file/css/style.css') }}">

</head>

<body>
  <div class="wrapper">
	<div class="container">
		<h1>Welcome</h1>
		
		<form class="form" method="POST" action="/app/registration">
			<input type="text" name="username" placeholder="User Name">
			<input type="text" name="fullname" placeholder="Fullname">
			<input type="email" name="email" placeholder="User Email">
			<input type="password" name="password" placeholder="Password">
            <input type="password" name="password1" placeholder="Confirm Password">
            <input type="text" name="phone" placeholder="Phone Number">
            <select name="user_type" >
                <option value="1">Patient</option>
                <option value="2">Doctor</option>
            </select><br>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
			<button type="submit" >Register</button>
<br><br>
                        <p>Already Have an Account? <a href="/">Login Now!</a> </p>
		</form>
	</div>
	

</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

  <script src="{{ asset('js/index.js') }}"></script>

</body>
</html>
