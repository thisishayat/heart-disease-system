<?php
/**
 * Created by PhpStorm.
 * User: hayat
 * Date: 12/15/17
 * Time: 3:18 AM
 */

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;

class SignUpSignIn
{
    public function signUp(Request $request){
        $input = $request->input();

        $usrTbl = new User();
        $usrTbl->username = $input['username'];
        $usrTbl->fullname = $input['fullname'];
        $usrTbl->email = $input['email'];
        $usrTbl->password = $input['password'];
        $usrTbl->phone_num = $input['phone'];
        $usrTbl->user_type = $input['user_type'];
        $save = $usrTbl->save();
        if($save){
            $request->session()->put('email', true);
            return view('templates.system.question');
        }
        return view('templates.system.index');
    }

    public function signIn(Request $request){
        $sessionData = session()->all();
        $input = $request->input();
        if(isset($sessionData['email']) && $sessionData['email'] == true){
            return view('templates.system.question');
        }
        $usrTbl = new User();
        $chk = $usrTbl->where('email',$input['username'])->get();
        if(count($chk) > 0){
            $request->session()->put('email', true);
            return view('templates.system.question');

        }
        $errors = 1;
        return view('templates.system.index',compact('errors'));


    }

    public function result(Request $request){
        $input = $request->input();
        return view('templates.system.result',compact('input'));
    }

    public function doctorHelp(Request $request){
        $input = $request->input();
        return view('templates.system.doctor-help',compact('input'));
    }
    public function chatWithDoctor(Request $request){
        $input = $request->input();
        return view('templates.system.chat-with-doctor',compact('input'));
    }

    public function doctorSuggestion(Request $request){
        $input = $request->input();
        return view('templates.system.doctor-suggestion',compact('input'));
    }

    public function patientProblem(Request $request){
        $input = $request->input();
        return view('templates.system.patients-problems-title',compact('input'));
    }

    public function submitSuccess(Request $request){
        $input = $request->input();
        return view('templates.system.problem-submit-success',compact('input'));
    }

}