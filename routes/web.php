<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $errors = 0;
    return view('templates.system.index',compact('errors'));
});
Route::get('/registration', function () {
    return view('templates.system.reg');
});

Route::get('/chat-with-patient', function () {
    return view('templates.system.chat-with-patient');
});


Route::post('app/result', 'SignUpSignIn@result');
Route::post('app/registration', 'SignUpSignIn@signUp');
Route::get('app/sigin', 'SignUpSignIn@signIn');
Route::get('app/doctor-help', 'SignUpSignIn@doctorHelp');
Route::get('/app/chat-with-doctor', 'SignUpSignIn@chatWithDoctor');
Route::get('/app/doctor-suggestion/1', 'SignUpSignIn@doctorSuggestion');
Route::get('/app/patitient-problems-title', 'SignUpSignIn@patientProblem');
Route::post('/app/problem-submit-success', 'SignUpSignIn@submitSuccess');
